# Build a Word Counting program

CC     = gcc
CFLAGS = -g -Wall

TARGET = wc

all: $(TARGET)

wc: wc.c
	$(CC) $(CFLAGS) -o $(TARGET) wc.c

clean:
	rm $(TARGET)

test:
	.wc testFile.txt
