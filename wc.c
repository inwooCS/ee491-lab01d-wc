/// Lab 01 - WordCount
///
/// @file wc.c
/// @version 2.0
///
/// @author In Woo Park <inwoo@hawaii.edu>
/// @brief  Lab 01d.2 - WordCount - EE 491F - Spr 2021
/// @date   1/26/2021
/// @see    https://www.opentechguides.com/how-to/article/c/72/c-file-counts.html
/// @note   If I could do this word count again I would definitely move all the gunk 
///		out of the main to make it cleaner and readable. 
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

/* 
* Main function with command line arguments as parameters.
* It will take in a file and return the line, word, and character count. 
* It will error check if the file is able to be read or not. 
*/
int main(int argc, char*argv[]) {

	/* determine how many arguments are passed in */
	int count = argc;

	/* check if --version is called for argc of 2 */
	if (count == 2 && !strcmp(argv[1], "--version")) {
		printf("wc 2.0\n");
		printf("Copyright (C) 2018 Free Software Foundation, Inc.\n");
		printf("Gitlab: <https://gitlab.com/inwooCS/ee491-lab01d-wc>.\n");
		printf("This is free software: you are free to change and redistribute it. Or give me money :)\n");
		printf("There is NO WARRANTY, to the extent permitted by law.\n\n");
		printf("Written, created, birthed, published, and developed by In Woo Park, the Overlord.\n");
	}

	/* error check for not enough arguments in argc. "usage" */
	if (argc < 2) {
		fprintf(stderr, "usage: wc FILE [FILE...]\n");
		exit(EXIT_FAILURE);
	}

	/* Separate variable for the file name */
	const char *filename = argv[count - 1];

	/* fp will use fopen to open and read argv[count - 1], the filename */
	FILE *filePointer = fopen(argv[count - 1], "r");

	/* error check if the file doesn't open and if arguments == 2*/
	if (count == 2 && filePointer == NULL) {
		fprintf(stderr, "wc: Can't open %s" "\n", filename);
		exit(EXIT_FAILURE);
	}

	/* error check if the file doesn't open and if arguments == 3*/
	if (count == 3 && filePointer == NULL) {
		fprintf(stderr, "wc: Can't open %s" "\n", filename);
		exit(EXIT_FAILURE);
	}

	/* Initialize Variables */
	int characters = 0;
	int words = 0;
	int lines = 0;

	/* Keeping track of the currentCharacter in an array */
	char currentCharacter;
	char characterArray [10000];

	/* index of character Array*/
	int i = 0;

	/* 
	 * @EOF, end of file
	 * While loop to loop through all the characters in the file.
	 * This loop keeps track of 2 counters (characters, lines)
	 * Stores all characters into an array
	 */
	while( (currentCharacter = fgetc(filePointer) ) != EOF) {

	/*increment if while loop succeeds*/
	characters++;

	/* Check new line */
	if (currentCharacter == '\n' || currentCharacter == '\0') {
		lines++;
	}

	/* if character is an alphabet, store into array */
	if (isalpha(currentCharacter)) {
		characterArray[i] = currentCharacter;
	}

	/* if character is a digit, store into array as a character */ 
	else if (isdigit(currentCharacter)) {
		characterArray[i] = currentCharacter;
	}

	/* other characters will store into the array as ' '*/
	else {
		characterArray[i] = ' ';
	}

		/* increment the index */
		i++;
	}

	/* Loop through the array and determine cases where current index 
	 * has a valid character and the next index does not. This determines 
	 * the word count. 
	 * J is current index, i is used for size. 
	 */
	for (int j = 0; j < i; j++) {

		/* increment words if condition applies */
		if (characterArray[j] != ' ' && characterArray[j + 1] == ' ') {
			words++;
		}
	}

	/* determine if additional arguments are provided 
	 * else print all 
	 */
	if(!strcmp(argv[1], "-c" ) || (!strcmp(argv[1], "--bytes")) ) {
		printf("%d %s\n", characters, filename);
	}

	else if (!strcmp(argv[1], "-l" ) || (!strcmp(argv[1], "--lines")) ) {
		printf("%d %s\n", lines, filename);
	}

	else if (!strcmp(argv[1], "-w" ) || (!strcmp(argv[1], "--words")) ) {
   		printf("%d %s\n", words, filename);
  	}

	else if (!strcmp(argv[1], "--version" )) {
		printf("wc 2.0\n");
		printf("Copyright (C) 2018 Free Software Foundation, Inc.\n");
		printf("Gitlab: <https://gitlab.com/inwooCS/ee491-lab01d-wc>.\n");
		printf("This is free software: you are free to change and redistribute it. Or give me money :)\n");
		printf("There is NO WARRANTY, to the extent permitted by law.\n\n");
		printf("Written, created, birthed, published, and developed by In Woo Park, the Overlord.\n");
	}

  	else {
	  	printf("%d  %d  %d  %s \n", lines, words, characters, filename);
  	}

	fclose(filePointer);
}
